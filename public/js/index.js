	var socket = io();

	socket.on('connect', () => {
		console.log('Connected to the server');

	});

	socket.on('disconnect', () => {
		console.log('Disconnected from the server');
	});

	socket.on('newMessage', (msg) => {
		var formattedTime = moment(msg.createdAt).format('h:mm a');
		var template = jQuery("#message-template").html();
		var html = Mustache.render(template,{
			from: msg.from,
			createdAt: formattedTime,
			text: msg.text
		});

		jQuery('#messages').append(html);

	});

	socket.on('newLocationMessage', (message) => {
		var formattedTime = moment(message.createdAt).format('h:mm a');
		var template = jQuery("#location-message-template").html();
		var html = Mustache.render(template, {
			from: message.from,
			url: message.url,
			createdAt: formattedTime
		});
		
		jQuery('#messages').append(html);

	});

	jQuery('#message-form').on('submit', (e) => {
		e.preventDefault();

		var messageTextbox = jQuery('[name=message]');
		socket.emit('createMessage', {
			from: 'User',
			text: messageTextbox.val()
		}, () => {
			messageTextbox.val('');
		});
	});

	var locationButton = jQuery('#sendLocation');
	locationButton.on('click', () => {
		if(!navigator.geolocation){
			return alert('Your browser does not support the location');
		}

		locationButton.attr('disabled','disabled');
		locationButton.text('Loading....');

		navigator.geolocation.getCurrentPosition((position) => {
			locationButton.removeAttr('disabled');
			locationButton.text('Send Location');

			socket.emit('createLocationMessage',{
				latitude: position.coords.latitude,
				longitude: position.coords.longitude
			});
		}, () => {
			locationButton.removeAttr('disabled');
			locationButton.text('Send Location');
			alert('Unable to fetch the location');
		});

	});