var expect = require('expect');

var {generateMessage, generateLocationMessage} =  require('./message');

describe('generateMessage', () =>  {
	it('Should generate message', () => {
		var from = 'Vishal';
		var text = 'Some Message';
		var createdAt = new Date().getTime();

		var message = generateMessage(from, text);
		expect(message).toEqual({ from, text ,createdAt});
	});
});

describe('generateLocationMessage', () => {
	if('should generate location message', () => {
		var from = 'User';
		var latitude = 17.6804639;
		var longitude = 74.018261;
		var url = `https://google.com/maps?q=${latitude},${longitude}`;
		var createdAt = new Date().getTime();

		var message = generateLocationMessage(from, latitude, longitude);
		expect(message).toEqual({from, url, createdAt});

	});
});

