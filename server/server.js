const socketIO = require('socket.io');
const http = require('http');
const express = require('express');
const path = require('path');
const publicPath = path.join(__dirname, '../public');

const {generateMessage, generateLocationMessage} = require('./utils/message');
const port = process.env.PORT || 3000;

var app = express();
var server = http.createServer(app);
var io = socketIO(server);

app.use(express.static(publicPath));

io.on('connection', (socket) => {
	console.log('New user connected');

	socket.on('disconnect', () => {
		console.log('User disconnected');
	});

	socket.emit('newMessage', generateMessage('Admin','Welcome to chat room.'));

	socket.broadcast.emit('newMessage', generateMessage('New User','New User joined the chat room'));


	socket.on('createMessage', (msg, callback) => {
		console.log('Create Message:', msg);
		io.emit('newMessage', generateMessage(msg.from,msg.text));

		callback();
	});

	socket.on('createLocationMessage', (coords) => {
		io.emit('newLocationMessage', generateLocationMessage('Location',coords.latitude,coords.longitude));
	});

});

server.listen(port, () => {
	console.log(`Server is listening to port ${port}...`);
});
